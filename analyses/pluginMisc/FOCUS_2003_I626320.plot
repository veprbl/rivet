BEGIN PLOT /FOCUS_2003_I626320/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D^0\to K^-K^-K^+\pi^+$
XLabel=$m_{K^+K^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I626320/d01-x01-y02
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-K^-K^+\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

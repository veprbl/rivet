BEGIN PLOT /SLD_1995_I378545/d02-x01-y01
Title=$1-T$ 
XLabel=$1-T$
YLabel=$1/\sigma\text{d}\sigma/\text{d}(1-T)$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d03-x01-y01
Title=Heavy Jet Mass
XLabel=$\rho_h$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\rho_h$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d04-x01-y01
Title=Total jet broadening
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d05-x01-y01
Title=Wide jet broadening
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d06-x01-y01
Title=Oblateness
XLabel=$O$
YLabel=$1/\sigma\text{d}\sigma/\text{d}O$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d07-x01-y01
Title=C Parameter
XLabel=$C$
YLabel=$1/\sigma\text{d}\sigma/\text{d}C$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d08-x01-y01
Title=Differential 2-jet rate(JADE) 
XLabel=$y_{23}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}y_{23}$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d12-x01-y01
Title=Differential 2-jet rate(Durham) 
XLabel=$y_{23}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}y_{23}$
END PLOT
BEGIN PLOT /SLD_1995_I378545/d14-x01-y01
Title=Energy-Energy correlation
XLabel=$\theta$ [degrees]
YLabel=EEC [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /SLD_1995_I378545/d15-x01-y01
Title=Asymmetry in Energy-Energy correlation
XLabel=$\theta$ [degrees]
YLabel=AEEC [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /SLD_1995_I378545/d16-x01-y01
Title=Jet Cone Energy Fraction
XLabel=$\theta$ [degrees]
YLabel=JCEF [$\text{rad}^{-1}$]
END PLOT
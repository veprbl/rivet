BEGIN PLOT /BELLE_2016_I1504055/
XLabel=$q^2$ [$\text{GeV}^2$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2016_I1504055/d01-x0[1,2]-y01
Title=$P_4^\prime$ in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$P_4^\prime$
END PLOT
BEGIN PLOT /BELLE_2016_I1504055/d01-x0[1,2]-y02
Title=$P_4^\prime$ in $B^+\to K^{*+}e^+e^-$
YLabel=$P_4^\prime$
END PLOT
BEGIN PLOT /BELLE_2016_I1504055/d01-x0[1,2]-y03
Title=$P_4^\prime$ in $B^+\to K^{*+}\mu^+\mu^-$
YLabel=$P_4^\prime$
END PLOT

BEGIN PLOT /BELLE_2016_I1504055/d01-x0[1,2]-y04
Title=$P_5^\prime$ in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$P_5^\prime$
END PLOT
BEGIN PLOT /BELLE_2016_I1504055/d01-x0[1,2]-y05
Title=$P_5^\prime$ in $B^+\to K^{*+}e^+e^-$
YLabel=$P_5^\prime$
END PLOT
BEGIN PLOT /BELLE_2016_I1504055/d01-x0[1,2]-y06
Title=$P_5^\prime$ in $B^+\to K^{*+}\mu^+\mu^-$
YLabel=$P_5^\prime$
END PLOT

BEGIN PLOT /BELLE_2016_I1504055/d02-x0[1,2]-y01
Title=$Q_4$ in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$Q_4$
END PLOT
BEGIN PLOT /BELLE_2016_I1504055/d02-x0[1,2]-y02
Title=$Q_5$ in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$Q_5$
END PLOT
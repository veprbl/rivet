BEGIN PLOT /BELLE_2009_I817326
XLabel=$q^2$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d0[1,2]-x0[1,2]-y01
YLabel=$\text{d}\mathcal{B}/\text{d}q^2\times10^7$ $\text{GeV}^{-1}$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d0[1,2]-x0[1,2]-y02
YLabel=$F_L$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d0[1,2]-x0[1,2]-y03
YLabel=$A_{FB}$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d0[1,2]-x0[1,2]-y04
YLabel=$A_{FB}$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d01-x0[1,2]-y01
Title=Differential branching ratio for $B\to K^* \ell^+\ell^-$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d02-x0[1,2]-y01
Title=Differential branching ratio for $B\to K \ell^+\ell^-$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d01-x0[1,2]-y02
Title=Longitudinal polarization for $B\to K^* \ell^+\ell^-$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d01-x0[1,2]-y03
Title=$A_{FB}$ for $B\to K^* \ell^+\ell^-$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d02-x0[1,2]-y03
Title=$A_{FB}$ for $B\to K \ell^+\ell^-$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d01-x0[1,2]-y04
Title=$A_{I}$ for $B\to K^* \ell^+\ell^-$
END PLOT
BEGIN PLOT /BELLE_2009_I817326/d02-x0[1,2]-y04
Title=$A_{I}$ for $B\to K \ell^+\ell^-$
END PLOT

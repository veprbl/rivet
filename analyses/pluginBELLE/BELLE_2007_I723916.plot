BEGIN PLOT /BELLE_2007_I723916/d01-x01-y01
Title=Helicity angle for $\Lambda_c(2880)^+\to\Sigma_c^{0,++}\pi^{+,-}$
XLabel=$\cos\theta_h$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2007_I723916/d01-x01-y02
Title=Angle between production and decay plane for $\Lambda_c(2880)^+\to\Sigma_c^{0,++}\pi^{+,-}$
XLabel=$\phi$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\phi$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2010_I879997/
LogY=0
END PLOT

BEGIN PLOT /BABAR_2010_I879997/d01-x01-y05
Title=$K^-\pi^+$ mass in $D^+\to K^-\pi^+ e^+\nu_e$
XLabel=$m_{K^-\pi^+}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BABAR_2010_I879997/d01-x01-y01
Title=$e^+\nu_e$ mass squared in $D^+\to K^-\pi^+ e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d01-x01-y02
Title=$\chi$ in $D^+\to K^-\pi^+ e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d01-x01-y03
Title=$\cos\theta_K$ in $D^+\to K^-\pi^+ e^+\nu_e$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d01-x01-y04
Title=$\cos\theta_e$ in $D^+\to K^-\pi^+ e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BABAR_2010_I879997/d02-x01-y01
Title=$e^+\nu_e$ mass squared in $D^+\to K^-\pi^+ e^+\nu_e$ ($m_{K\pi}<0.8$\,GeV)
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d02-x01-y02
Title=$\chi$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($m_{K\pi}<0.8$\,GeV)
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d02-x01-y03
Title=$\cos\theta_K$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($m_{K\pi}<0.8$\,GeV)
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d02-x01-y04
Title=$\cos\theta_e$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($m_{K\pi}<0.8$\,GeV)
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BABAR_2010_I879997/d03-x01-y01
Title=$e^+\nu_e$ mass squared in $D^+\to K^-\pi^+ e^+\nu_e$ (0.8<$m_{K\pi}<0.9$\,GeV)
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d03-x01-y02
Title=$\chi$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.8<m_{K\pi}<0.9$\,GeV)
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d03-x01-y03
Title=$\cos\theta_K$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.8<m_{K\pi}<0.9$\,GeV)
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d03-x01-y04
Title=$\cos\theta_e$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.8<m_{K\pi}<0.9$\,GeV)
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BABAR_2010_I879997/d04-x01-y01
Title=$e^+\nu_e$ mass squared in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.9<m_{K\pi}<1.0$\,GeV)
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d04-x01-y02
Title=$\chi$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.9<m_{K\pi}<1.0$\,GeV)
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d04-x01-y03
Title=$\cos\theta_K$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.9<m_{K\pi}<1.0$\,GeV)
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d04-x01-y04
Title=$\cos\theta_e$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($0.9<m_{K\pi}<1.0$\,GeV)
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BABAR_2010_I879997/d05-x01-y01
Title=$e^+\nu_e$ mass squared in $D^+\to K^-\pi^+ e^+\nu_e$ ($1.0<m_{K\pi}<1.6$\,GeV)
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d05-x01-y02
Title=$\chi$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($1.0<m_{K\pi}<1.6$\,GeV)
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d05-x01-y03
Title=$\cos\theta_K$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($1.0<m_{K\pi}<1.6$\,GeV)
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT
BEGIN PLOT /BABAR_2010_I879997/d05-x01-y04
Title=$\cos\theta_e$ in $D^+\to K^-\pi^+ e^+\nu_e$ ($1.0<m_{K\pi}<1.6$\,GeV)
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BABAR_2012_I1125973/d01-x01-y01
Title=$B^0\to \pi^-\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{7} \mathrm{d}\mathrm{Br}(B^0\to\pi^-\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125973/d01-x01-y02
Title=$B^0\to \pi^-\ell^+\nu$ (no FSR)
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{7} \mathrm{d}\mathrm{Br}(B^0\to\pi^-\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125973/d02-x01-y01
Title=$B^+\to \pi^0\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{7} \mathrm{d}\mathrm{Br}(B^+\to\pi^0\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125973/d02-x01-y02
Title=$B^+\to \pi^0\ell^+\nu$ (no FSR)
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{7} \mathrm{d}\mathrm{Br}(B^+\to\pi^0\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125973/d03-x01-y01
Title=$B^+\to \omega\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{7} \mathrm{d}\mathrm{Br}(B^+\to\omega\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125973/d04-x01-y01
Title=$B^+\to \eta\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{7} \mathrm{d}\mathrm{Br}(B^+\to\eta\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
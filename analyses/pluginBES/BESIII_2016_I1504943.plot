BEGIN PLOT /BESIII_2016_I1504943/d01-x01-y01
Title=Inclusive branching ratio for $\eta^\prime\to\gamma\gamma\pi^0$ decays
XLabel=
YLabel=$\text{Br}\times10^{-4}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1504943/d01-x01-y02
Title=Branching ratio for $\eta^\prime\to\gamma\omega(\to\gamma\pi^0)$ decays
XLabel=
YLabel=$\text{Br}\times10^{-4}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1504943/d01-x01-y03
Title=Non-resonance branching ratio for $\eta^\prime\to\gamma\gamma\pi^0$ decays
XLabel=
YLabel=$\text{Br}\times10^{-4}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1504943/d02-x01-y01
Title=$\gamma\gamma$ mass distribution in $\eta^\prime\to\gamma\gamma\pi^0$ decays
XLabel=$m^2_{\gamma\gamma}$ [$\text{GeV}^2$]
YLabel=$\text{d}\Gamma/\text{d}m^2_{\gamma\gamma}$ [$\text{keV}/\text{GeV}^2$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2013_I1209121/d01-x01-y01
Title=$\eta\eta$ mass distribution in $J/\psi\to\gamma\eta\eta$
XLabel=$m_{\eta\eta}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\eta\eta}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1209121/d01-x01-y02
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma\eta\eta$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1209121/d01-x01-y03
Title=$\cos\theta_\eta$ distribution in $J/\psi\to\gamma\eta\eta$
XLabel=$\cos\theta_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\eta$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1209121/d01-x01-y04
Title=$\phi_\eta$ distribution in $J/\psi\to\gamma\eta\eta$
XLabel=$\phi_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_\eta$
LogY=0
END PLOT

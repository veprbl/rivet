Name: BESIII_2021_I1870322
Year: 2021
Summary: Mass distributions in the decay $D^+_s\to\pi^+\pi^+\pi^-\eta$
Experiment: BESIII
Collider: BEPC
InspireID: 1870322
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 104 (2021) 071101
RunInfo: Any process producing D_s+ mesons
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to\pi^+\pi^+\pi^-\eta$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021aza
BibTeX: '@article{BESIII:2021aza,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Study of the Decay $D^{+}_{s}\rightarrow \pi^{+}\pi^{+}\pi^{-}\eta$ and Observation of the W-annihilation Decay $D^{+}_{s}\rightarrow a_0(980)^+\rho^0$}",
    eprint = "2106.13536",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.104.L071101",
    journal = "Phys. Rev. D",
    volume = "104",
    pages = "071101",
    year = "2021"
}
'

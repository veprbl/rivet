BEGIN PLOT /BESIII_2021_I1908066/d01-x01-y01
Title=$\sigma(e^+e^-\to J/\psi+X)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to J/\psi+X))$
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1908066/d02-x01-y01
Title=$\sigma(e^+e^-\to J/\psi+X)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to J/\psi+X))$
ConnectGaps=1
END PLOT

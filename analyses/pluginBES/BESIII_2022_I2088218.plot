BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y01
Title=$K^+\pi^-$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y02
Title=$K^+\pi^0$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y04
Title=$\pi^+\pi^0$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y05
Title=$\pi^-\pi^0$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y06
Title=$K^+\pi^-\pi^0$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y07
Title=$\pi^+\pi^-\pi^0$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y08
Title=$K^+\pi^+$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y09
Title=$K^+\pi^+\pi^0$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2088218/d01-x01-y10
Title=$K^+\pi^+\pi^-$ mass distribution in $D^+_s\to K^+\pi^+\pi^-\pi^0$
XLabel=$m_{K^+\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

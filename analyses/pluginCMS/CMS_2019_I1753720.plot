# BEGIN PLOT /CMS_2019_I1753720/d01-x01-y01
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ all-jets fiducial PI cross section
XLabel=Fiducial PI phase space
YLabel=cross section (pb)
PlotSize=10.7,7.2
LogY=0
# END PLOT

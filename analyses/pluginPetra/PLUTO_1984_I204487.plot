BEGIN PLOT /PLUTO_1984_I204487/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.2$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\text{d}\sigma(\gamma\gamma\to \pi^+\pi^-)/d\cos\theta$ [nb]
ConnectGaps=1
END PLOT

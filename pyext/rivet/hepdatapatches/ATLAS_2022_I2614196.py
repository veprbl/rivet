
def patch(path, ao):
    ao.rmAnnotation('ErrorBreakdown')
    needs_patching = [
        '/REF/ATLAS_2022_I2614196/d15-x01-y01',
        '/REF/ATLAS_2022_I2614196/d16-x01-y01',
    ]
    if path in needs_patching:
      out = [ ]
      for idx in range(5):
          newpath = path.replace('x01', 'x0' + str(idx+1))
          newao = ao.clone()
          newao.setPath(newpath)
          toKeep = [ ]
          pidx = newao.numPoints()
          while pidx > 0:
              toKeep.append( pidx - (5 - idx) )
              pidx -= 5
          toRemove = [ p for p in range(newao.numPoints()) if p not in toKeep ]
          toRemove.reverse()
          for p in toRemove:
              newao.rmPoint(p)
          out.append(newao)
      return out
    return ao

